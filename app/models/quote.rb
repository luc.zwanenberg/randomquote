class Quote < ApplicationRecord

    validates :text, presence: true,
        length: { minimum: 5 }
        
    validates :author, presence: true,
        length: { minimum: 5 }
    
end
