class Front::QuotesController < Front::ApplicationController

  def random
    @quote = Quote.random

    render :json => {
      text: @quote.text,
      author: @quote.author
    }
  end

end
