class Admin::QuotesController < Admin::ApplicationController
    
    def index
        @quotes = Quote.all    
    end

    def new
        @quote = Quote.new
    end

    def create
        @quote = Quote.new(quote_parameters)
        if @quote.save
            redirect_to admin_quotes_path
        else
            render 'new'
        end
    end

    def edit
        @quote = Quote.find(params[:id])
    end

    def update
        @quote = Quote.find(params[:id])
       
        if @quote.update(quote_parameters)
          redirect_to admin_quotes_path
        else
          render 'edit'
        end
    end

    def destroy
        @article = Quote.find(params[:id])
        @article.destroy
     
        redirect_to admin_quotes_path
    end
    
    private def quote_parameters
        params.require(:quote).permit(:text, :author)
    end

end