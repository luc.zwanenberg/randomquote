import React from "react"
import PropTypes from "prop-types"
import Quote from "../resources/Quote";

class RandomQuote extends React.Component {

  constructor() {
    super();

    this.state = {
      quote: null,
      loading: true
    };
  }

  componentDidMount() {
    this.loadQuote();
  }

  loadQuote() {
    this.setState({
      loading: true,
    });

    Quote.random().then(quote => {
      this.setState({
        loading: false,
        quote
      });
    }).catch(error => {
      this.setState({
        loading: false,
        quote: null
      });

      console.error(error);
    });
  }

  render () {
    let loading = this.state.loading !== false;
    let quote = this.state.quote;

    return (
      <div className={ "quote-wrapper " + (loading ? 'loading' : 'loaded') }>
        <div className="quote-loader">
          Loading...
        </div>
        <div className="quote">
          <div className="quote-top">
              <h2>{ quote ? `"${quote.text}"` : <i>No quote to display.</i> }</h2>
          </div>
          <div className="quote-bottom">
            <h3>{ quote && `- ${quote.author} -` }</h3>
          </div>
          <div className="quote-button">
            <button type="button" className="button" onClick={ this.loadQuote.bind(this) }>New quote</button>
          </div>
        </div>
      </div>
    )
  }

}

export default RandomQuote
