import axios from 'axios-on-rails'

export default class Quote {

  constructor(data) {
    this.author = data.author ? data.author : null;
    this.text = data.text ? data.text : null;
  }

  static random() {
    return new Promise((resolve, reject) => {
      let url = 'quotes/random';
      
      axios.get(url).then((response) => {
        let quote = new Quote(response.data);
        resolve(quote);
      }).catch(reject);
    });
  }

}
