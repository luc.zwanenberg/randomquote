# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

quotes = [
    ["Together we can change the world, just one random act of kindness at a time.", "Ron Hall"],
    ["I don't believe that if you do good, good things will happen. Everything is completely accidental and random. Sometimes bad things happen to very good people and sometimes good things happen to bad people. But at least if you try to do good things, then you're spending your time doing something worthwhile.", "Helen Mirren"],
    ["Don't cry because it's over, smile because it happened.", "Dr. Seuss"],
    ["I'm selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can't handle me at my worst, then you sure as hell don't deserve me at my best.", "Marilyn Monroe"],
    ["Be yourself; everyone else is already taken.", "Oscar Wilde"],
    ["Alcohol may be man's worst enemy, but the bible says love your enemy.", "Frank Sinatra"],
    ["When you reach the end of your rope, tie a knot in it and hang on.", "Franklin D. Roosevelt"],
    ["It is during our darkest moments that we must focus to see the light", "Aristotle"],
    ["Find a place inside where there's joy, and the joy will burn out the pain.", "Joseph Campbell"],
    ["Nothing is impossible, the word itself says 'I'm possible'!", "Audrey Hepburn"],
    ["Don't judge each day by the harvest you reap but by the seeds that you plant.", "Robert Louis Stevenson"],
    ["The only thing necessary for the triumph of evil is for good men to do nothing.", "Edmund Burke"],
    ["One of the most beautiful qualities of true friendship is to understand and to be understood.", "Lucius Annaeus Seneca"],
    ["Success is not final, failure is not fatal: it is the courage to continue that counts.", "Winston Churchill"],
]

quotes.each do |text, author|
    Quote.create( text: text, author: author )
end