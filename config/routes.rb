# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

Rails.application.routes.draw do

  namespace :admin do
    resources :quotes

    get '/', to: redirect('admin/quotes')
  end

  scope module: 'front' do
    get '/quotes/random', to: 'quotes#random'

    root 'home#index'
  end

end
